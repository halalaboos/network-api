package net.halalaboos.network.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NetworkUtils {

	public static void writeString(DataOutputStream outputStream, String text) throws IOException {
		char[] chars = text.toCharArray();
		outputStream.writeByte(chars.length);
		for (char c : chars) {
			outputStream.writeChar(c);
		}
	}
	
	public static String readString(DataInputStream inputStream) throws IOException {
		byte length = inputStream.readByte();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = inputStream.readChar();
		}
		return String.valueOf(text);
	}
}
