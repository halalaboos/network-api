package net.halalaboos.network.tcp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class ConnectionHandler <T extends ClientHandler> extends Thread {

	private final Server server;

	private final ServerSocket serverSocket;
	
	private final List<T> clients = new CopyOnWriteArrayList<T>();
	
	private boolean run = false;
	
	public ConnectionHandler(Server server, ServerSocket serverSocket) {
		this.server = server;
		this.serverSocket = serverSocket;
	}
	
	@Override
	public void run() {
		try {
			this.run = true;
			while (server.isOnline() && run) {
				try {
					Socket socket = serverSocket.accept();
					T handler = generateHandler(server, socket);
					if (shouldAllowConnect(handler))
						handler.start();
					else
						handler.close();
				} catch (IOException e) {
					System.err.println("CLIENT CON ERR: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			
		} finally {
			cleanup();
		}
	}

	protected abstract boolean shouldAllowConnect(T handler);
	
	protected abstract T generateHandler(Server server, Socket socket) throws IOException;

	public void close() {
		run = false;
	}
	
	public void broadcast(int id, Object data) {
		broadcast(null, id, data);
	}
	public void broadcast(T sender, int id, Object data) {
		for (T handler : clients) {
			if (sender != handler)
				handler.write(id, data);
		}
	}
	
	private void cleanup() {
		for (T handler : clients) {
			handler.close();
		}
	}

	public List<T> getClients() {
		return clients;
	}
}
