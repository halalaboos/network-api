package net.halalaboos.network.api;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface Packet <T> {
		
	public void write(Handler handler, T data, DataOutputStream outputStream) throws IOException;
	
	public T read(Handler handler, DataInputStream inputStream) throws IOException;

}
