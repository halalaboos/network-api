package net.halalaboos.network.api;

public class ProtocolHandler {
	
	private final Packet[] packets = new Packet[256];
	
	private byte packetCount = 0;
	
	public ProtocolHandler() {
	}
	
	
	public void read(byte id, Handler handler) {
		Packet packet = packets[id];
		if (packet != null) {
			try {
				Object data = packet.read(handler, handler.getInputStream());
				handler.onRecievedPacket(packet, data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void write(int id, Object data, Handler handler) {
		try {
			Packet packet = packets[id];
			if (packet != null) {
				handler.getOutputStream().writeByte(id);
				packet.write(handler, data, handler.getOutputStream());
			}
			handler.getOutputStream().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setup() {

	}
	
	protected byte addPacket(Packet packet) {
		byte id = packetCount;
		packets[id] = packet;
		packetCount++;
		return id;
	}
	
}
